package com.oreillyauto.fastsort;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Map;
import java.util.TreeMap;

public class FastSort {

    public static void main(String[] args) throws Exception {
        if (args.length == 3) {

            File file1 = new File(args[0]);
            File file2 = new File(args[1]);
            File file3 = new File(args[2]);
            File sorted1 = new File(file1.getAbsolutePath().replaceAll(".txt", ".sorted.txt"));
            File sorted2 = new File(file2.getAbsolutePath().replaceAll(".txt", ".sorted.txt"));
            File sorted3 = new File(file3.getAbsolutePath().replaceAll(".txt", ".sorted.txt"));

            System.out.println("Process started");

            cleanup(new File[] { sorted1, sorted2, sorted3 });

            File out1 = new File(file1.getAbsolutePath().replaceAll(".txt", ".sorted.txt"));
            File out2 = new File(file2.getAbsolutePath().replaceAll(".txt", ".sorted.txt"));
            File out3 = new File(file3.getAbsolutePath().replaceAll(".txt", ".sorted.txt"));

            long start = System.currentTimeMillis();

            sort(file1, out1);
            sort(file2, out2);
            sort(file3, out3);

            long end = System.currentTimeMillis();
            System.out.println("Process Time: " + (end - start) + " milliseconds");
            System.out.println("Done.");
        } else {
            System.out.println("Usage: java -jar class <file1> <file2> <file3>");
        }
    }

    private static void cleanup(File[] fileArray) {
        for (File file : fileArray) {
            if (file.exists()) {
                file.delete();
            }
        }
    }

    private static void sort(File in, File out) throws Exception {
        BufferedWriter writer = null;
        StringBuilder sb = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(in))) {
            writer = new BufferedWriter(new FileWriter(out));
            String line;
            Map<Integer, Integer> myMap = new TreeMap<Integer, Integer>();
            while ((line = br.readLine()) != null) {
                myMap.put(Integer.parseInt(line), Integer.parseInt(line));
            }
            for (Map.Entry<Integer, Integer> entry : myMap.entrySet()) {
                Integer key = entry.getKey();
                sb.append(key + "\n");
            }
            writer.write(sb.toString());
            writer.flush();

        }
        catch (Exception e) {
            e.printStackTrace();
        } finally {
            writer.close();
        }
    }
}